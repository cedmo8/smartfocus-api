﻿#Load Config
$erroractionpreference = "Stop"
. "C:\API\Smart Focus\Config\REACTIV_A.ps1"


#Open Session
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"
'Connection '+$r.response.responseStatus


[xml]$SOAP = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:api="http://api.service.apiccmd.emailvision.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <api:getMessageSummaryList>
         <token>'+$token+'</token>
         <listOptionsEntity>
            <page>1</page>
            <pageSize>10</pageSize>
            <sortOptions>
               <sortOption>
                  <column>createdDate</column>
                  <order>DESC</order>
               </sortOption>
            </sortOptions>
         </listOptionsEntity>
      </api:getMessageSummaryList>
   </soapenv:Body>
</soapenv:Envelope>'


$URI = "http://p5apie.emv3.com/apiccmd/services/CcmdService?wsdl"
[xml]$s = Invoke-WebRequest $uri -Method post -ContentType 'text/xml' -Body $SOAP -DisableKeepAlive


$data = $s.envelope.body.getMessageSummaryListResponse.return.messageSummaryList.messageSummary | Select-Object name,messageid,subject,createdDate

$data2 = $data | select -First 1

[string]$data2.messageid+': '+[string]$data2.name








#Close Session
[xml]$r=Invoke-RestMethod -Uri https://$server/apiccmd/services/rest/connect/close/$token
$r.response.result."#text"