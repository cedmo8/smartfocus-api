#Clean Varables
Remove-Variable * -ErrorAction SilentlyContinue

#Reset Report
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = @"
UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report
SET 
        UnsubDownload='0',
        BounceDownload='0',
        StatusDownload='Error',
        UnsubUpload='0',
        BounceUpload='0',
        StatusUpload='Error',
        CountStart='0',
        CountEnd='0',
        CountDiff='0'
GO
"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query

#Create Hash Table of Accounts and Segment
$accountlist = [ordered]@{
    'REACTIV_A'       = '1943415'
#    'REACTIV_B'       = '1943345'
#    'REACTIV_C'       = '3'
#    'REACTIV_D'       = '4'
#    'REACTIV_E'       = '407483'
#    'REACTIV_F'       = '6'
#    'REACTIV_G'       = '411414'
    'REACTIV_NEW'     = '1990039'
#    'REACTIV_MED'     = '9'
#    'REACTIV_WELCOME' = '10'
    'REACTIV_PRE'     = '3507798','3508081'
#    'REACTIV_TWO'     = '12'
    }


#Loop Throught Accounts
ForEach ($account in $accountlist.GetEnumerator()) {


#Start Job for Account
Start-Job -Name $account.name -ArgumentList $account.name,$account.value -ScriptBlock {


#Collect Varables for Job
$accountname = $args[0]
$segmentid = $args[1]
$erroractionpreference = "Stop"
Import-Module sqlps -WarningAction SilentlyContinue
Write-Output "`nAccount Name: $accountname`nSegment ID: $($segmentid[0])"

#TEST
#. "C:\API\Smart Focus\Config\REACTIV_PRE.ps1"
#$segmentid = "1943345"
#$accountname = "REACTIV_PRE"


#Load Account Configuration
Write-Output "Loading Config for: $accountname"
try {. "C:\API\Smart Focus\Config\$accountname.ps1"}
catch {
    Write-Output "Error loading Config for $accountname Check C:\API\Smart Focus\Config`n"
    #sql set status to Config Error
    $sqlserver = 'SQL-API'
    $database = 'SMARTFOCUS'
    $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusDownload='Config' WHERE Account = '$accountname'"

    $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query
    return
    }
    Write-Output "Config Loaded Seccussfuly for Account: $accountname Login: $login"


#Open Session
Write-Output "Connecting to $accountname"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token  = $r.response.result."#text"
Write-Output "Connection $($r.response.responseStatus), Token: $($token -replace '(.{4}).+','$1')"

$segmentid -is [system.array]

IF ($segmentid -is [system.array]) {

$unsubid = $segmentid[0]
$bounceid = $segmentid[1]
$arraymode = $true

#here to swap segment ID
#Load Segment and Display Name
Write-Output "Loading Unsub Segment Info: $unsubid"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/segmentationservice/$token/segment/$unsubid
Write-Output "Unsub Segment Name: $($r.response.segmentation.name)"
#Load Segment and Display Name
Write-Output "Loading Bounce Segment Info: $bounceid"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/segmentationservice/$token/segment/$bounceid
Write-Output "Bounce Segment Name: $($r.response.segmentation.name)"

} ELSE {

#here to swap segment ID
#Load Segment and Display Name
Write-Output "Loading Segment Info: $segmentid"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/segmentationservice/$token/segment/$segmentid
Write-Output "Segment Name: $($r.response.segmentation.name)"

}




#Start Unsubs,Hardbounces

 

foreach ($type in 'UNJOIN_MEMBERS','QUARANTINED_MEMBERS') {


IF ($type -eq 'UNJOIN_MEMBERS'){
$typename = 'Unsub'
$sourceapp = 1
Write-Output "The Type is: $type"

IF ($arraymode -eq $true) {

$segmentid = $unsubid
}}

IF ($type -eq 'QUARANTINED_MEMBERS'){
$typename = 'Bounce'
$sourceapp = 2
Write-Output "The Type is: $type"

IF ($arraymode -eq $true) {

$segmentid = $bounceid

}}




#Create Download and set Export ID
Write-Output "Creating Download for SegmentID: $segmentid Type: $type"
[xml]$r=Invoke-RestMethod https://$server/apiexport/services/rest/createDownloadByMailinglist/$token/$segmentid/$type/EMAIL/TRUE/EMAIL/TRUE/CSV/?dateFormat=MM/dd/yyyy
$exportid = $r.response.result."#text"
Write-Output "Creation $($r.response.responseStatus), ExportID: $exportid"

$filestatus = $null
#Loop Until Ready and then Download File - add time out at some point
Write-Output "Downloading File: $exportid Type: $type"
$timeout = New-TimeSpan -Minutes 15
$timeout = New-TimeSpan -Seconds 10
$sw = [Diagnostics.Stopwatch]::StartNew()

while($filestatus -ne "OK")
    {
        IF ($sw.Elapsed -gt $timeout) 
            {
                Write-Output "The download for ExportID: $exportid Type: $type Timed out after $($Timeout.TotalMinutes) Minutes"

                #sql set status to time out and highlite yellow
                $sqlserver = 'SQL-API'
                $database = 'SMARTFOCUS'
                $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusDownload='Timeout' WHERE Account = '$accountname'"

                $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query

                #Exit the entire job and mark it as Timeout
                return
            }

        Start-Sleep -s 10

        [xml]$r=Invoke-RestMethod https://$server/apiexport/services/rest/getDownloadFile/$token/$exportid
        $filestatus = $r.response.apiDownloadFile.filestatus

            IF ($filestatus -ne "OK")
                {
                    Write-Output "$type File Not Ready($($sw.Elapsed.TotalSeconds) Seconds Elapsed), Retrying"
                }
    }#Download File End
$sw.Stop()


#Convert Data to CSV and Report Record Count
$download = convertfrom-csv -inputobject $r.response.apiDownloadFile.fileContent -delimiter ","
Write-Output "$type File Download Complete.`nTime Elapsed: $($sw.Elapsed.TotalSeconds) Seconds`nRecord Count: $($download.count)"


#Insert Data into SQL Table
$download | ForEach-Object {

#SQL Insert
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$date = (Get-Date).ToString('yyyy/MM/dd')
$query = @"
INSERT INTO SMARTFOCUS.dbo.SF_Unsubscription_Sync
           ([Email]
           ,[SourceApp]
           ,[DateUnjoin]
           ,[Origin])
     VALUES
           ('$($_.EMAIL)'
           ,'$sourceapp'
           ,'$date'
           ,'$accountname')
"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query


}#Insert Data End


#Update Report with Download Statistics
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = @"
UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report
SET 
	       $($typename)Download='$($download.count)',
	       StatusDownload='Complete'
WHERE Account = '$accountname'
GO
"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query
}#End UNSUB/HB Loop


#Close Session
Write-Output "Closing Session $accountname"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/close/$token
Write-Output "$accountname $($r.response.result."#text")`n"
}}


#Retreve Results fron Job
$results = $null
Write-Output "`nWaiting for Jobs...  (Timeout in 35 Minutes)"
Get-Job | Wait-Job -Timeout 2100 | Receive-Job | ForEach-Object { ##timeout should be ~2.1 x the time of the download wait time
    $results +=  "$_`n"
    #Remove-Job $_ -Force
}
Get-job | Remove-Job -Force

#report results test
$results

$logdate = (Get-Date).ToString('yyyyMMdd')
$logfile = "\\Sql-api\api\Smart Focus\Logs\UnsubscribeSync\$logdate.Log"
#https://social.technet.microsoft.com/forums/windowsserver/en-US/3ea404aa-74d5-4c73-b6b7-8707bc560e23/invokesqlcmd-is-messing-with-my-providers
$results | Out-File "Microsoft.PowerShell.Core\FileSystem::$logfile"

#SQL Download Report - Change to update - maby better as a hast table if can be made globaley
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = "EXEC SMARTFOCUS.dbo.SF_Unsubscription_SendReport @logfile = '$logfile'"
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query


return


#TODO: make detailed Documentation for this process and host on Twiki
#ToDo: add documentation button or link in error explanation
#ToDo: Create visio document sshoecasing how the processs workss including the mutiple threads for downloads and uploads, and smaller details such as "count number of records in download andd write to log and read segment name and id and write to log
#ToDo: have subject line change to corrispond to the warning state e.g. (Error)"
#ToDo: Documentation should include casveat about how anythidng that john unsubscribes will not be downloaded by this process as he marked the records with a 1 meaning that they are alredy held localy
#ToDo: Create a History table of all records downloaded brefore dedupe and a history of all records uploaded after dedupe include date for both
#ToDo: have emv return the logs from the file upload/import and store thease on the api server  with a link to them in the report
#ToDo: have the report contain a list of failed uploads such as syntax errors ect.
#ToDo: find a way to allow the process to remember the date and time it left off so i can be run agaijn and again withour performing dupicate work, manly an issue for selecting midas data to upload as accounts are marken with a 1/2, perhaps use this marking methord in the unsub table or use the history table to dedupe the selection againsa however that does not cover the event that an account times out anas it wwill then not recieve the ollder datta unless the history is saved on a per account basis and only written to/marked after a secussfull upload
#ToDo: reporting services to tract history asnd trends, will require that the reports table be copyes to an archive table with a date stamp
#ToDo: add coloured output for errors to match the colour codes of the report
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: 


<#

'



[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/segmentationservice/$token/segment/1947766/count
$r.response.result."#text"


$date = (Get-Date).ToString('yyyy-MM-dd-00:00:00')


[xml]$r=Invoke-RestMethod https://$server/apireporting/services/rest/getListSizeMonthlyReportByDateRange/$token/$date/$date
$r.response.result.list.unsubscribedMembers




#SQL Insert
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = @"
INSERT INTO [dbo].[SF_Unsubscription_Sync]
           ([Email]
           ,[SourceApp]
           ,[DateUnjoin]
           ,[Origin])
     VALUES
           ('Clinton.Edmondson@reactiv.co.uk'
           ,'1'
           ,'2015/09/04'
           ,'$accountname')
"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query



12*3





#Load Config
$erroractionpreference = "Stop"
Import-Module sqlps -WarningAction SilentlyContinue
. "C:\API\Smart Focus\Config\REACTIV_A.ps1"


#Open Session
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"







#Insert Data into SQL Table
$download = convertfrom-csv -inputobject $r.response.apiDownloadFile.fileContent -delimiter ","




$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'


$query = @"
INSERT INTO [dbo].[SF_Unsubscription_Sync]
           ([Email]
           ,[SourceApp]
           ,[DateUnjoin]
           ,[Origin])
     VALUES
           ('Clinton.Edmondson@reactiv.co.uk'
           ,'1'
           ,'2015/09/10'
           ,'REACTIV_A')
"@

$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query





$database = 'API'
$sqlserver = '.'
$table = 'dbo.SF_Wills_Opens_Import'

$download | ForEach-Object {

  $EMAIL = $_.EMAIL
  $CAMPAGNE_ID = $_.CAMPAGNE_ID

Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query "insert into $table VALUES ('$CAMPAGNE_ID','$EMAIL','Wills')"
}#Insert Data End



#Close Session
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/close/$token
$r.response.result."#text"







###########################################

#TEST SQL Upload Report TEST
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = @"
INSERT INTO SMARTFOCUS.dbo.SF_Unsubscription_Report
           ([Account]
	       ,[UnsubDownload]
	       ,[BounceDownload]
	       ,[StatusDownload]
	       ,[UnsubUpload]
           ,[BounceUpload]
           ,[StatusUpload]
           ,[CountStart]
           ,[CountEnd]
           ,[CountDiff])
     VALUES
           ('$accountname'
           ,'$($download.count)'
           ,'0'
           ,'Complete'
           ,'0'
           ,'0'
           ,'Error'
           ,'0'
           ,'0'
           ,'0')





"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query


#SQL Send report
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = @"
exec stored procedure to collate report and email it, also to zero the report to protect against failed accoutns)
also do a check that all accoutns seccedded by populating and checking a compleated cell in the report table
"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query


#execute stored procedure in centre of file to process and deduplicate all downloaded files and redy the data befor running the second falf (upload) and then run another stored procedure to generate the report and cleanup, add account for MIDAS to reprisent the sunubscriptions held localy and tract how many where aquired from there

#find way to handle last run time to allow the process to run any time and only process new records but not fault if left for a while



#>