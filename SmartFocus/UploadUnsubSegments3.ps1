﻿######################
### Upload Process ###
######################

Remove-Variable * -ErrorAction SilentlyContinue

$accountlist = [ordered]@{
    'REACTIV_A'       = '1943415'
    #'REACTIV_B'       = '1943345'
    #'REACTIV_C'       = '1960364'
    #'REACTIV_D'       = '1960414'
    #'REACTIV_E'       = '407483'
    #'REACTIV_F'       = '328435'
    #'REACTIV_G'       = '411414'
    #'REACTIV_NEW'     = '1990039'
    #'REACTIV_MED'     = '1113670230','1113671069'
    #'REACTIV_WELCOME' = '1991232','1991237'
    #'REACTIV_PRE'     = '3507798','3508081'
    #'REACTIV_TWO'     = '3513781','3513782'
    }




#Write-Output "######################`n### Upload Process ###`n######################" | Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$logfile"


#Loop Throught Accounts
ForEach ($account in $accountlist.GetEnumerator()) {


    #Start Job for Account
    Start-Job -Name $account.name -ArgumentList $account.name -ScriptBlock {

        #Collect Varables for Job
        $accountname = $args[0]
        $erroractionpreference = "Stop"

        $sqlserver = 'SQL-API'
        $database = 'SMARTFOCUS'


        #$accountname = 'REACTIV_A'


        Write-Output "`n########## Upload Process START for $accountname ##########"
        Write-Output "Account Name: $accountname"


        #Load Account Configuration
        Write-Output "$accountname`: Loading Configuration File"

        TRY {
                . "C:\API\Smart Focus\Config\$accountname.ps1"
            }

        CATCH
            {
                Write-Output "$accountname`: Error loading Configuration for $accountname Check C:\API\Smart Focus\Config"
                Write-Output "########## Upload Process END for $accountname ##########`n"
                #sql set status to Config Error
                $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusUpload='Config' WHERE Account = '$accountname'"

                $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query
                return
            }
            Write-Output "$accountname`: Configuration Loaded Seccussfuly Login: $login"


            #Open Session
            Write-Output "$accountname`: Connecting to Account"
            [xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
            $token  = $r.response.result."#text"
            $responseStatus = $r.response.responseStatus
            $tokenreport = $token -replace '(.{4}).+','$1'
            Write-Output "$accountname`: Connection $responseStatus, Token: $tokenreport"

            #ToDo: capture connection response and throw/return with error if not seccus or is failure - maby make catches for all API calls


            #Create Boundry
            $set = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray()
            [string]$boundry = $null
            for ($x = 0; $x -lt 22; $x++) {
                $boundry += $set | Get-Random
            }


            $query = @"
            SELECT TOP 15
                Email,SourceApp,CONVERT(VARCHAR(19), DATEADD(HH,+1,DateUnjoin), 120) 
                FROM SMARTFOCUS.[dbo].[SF_Unsubscription_Sync]
                WHERE email like '%reactiv%'

"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query | ConvertTo-Csv -NoTypeInformation | Select-Object -Skip 1


            #Set Output field separator line break insted of space (used in string concatination for encoding)
            $OFS = "`r`n"

            #Encode Data
            TRY {
                    $encodedata = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($sql))
                }

            #ToDo: the encode statment failes if there is no data so need to add a catch for "Exception calling "GetBytes" with "1" argument(s): "Array cannot be null."
            CATCH [System.Management.Automation.MethodInvocationException]
                {
                    $errormessage = $_.exception.message
                    Write-Output "Data encoding Failed with the Error: $errormessage"
                    IF ($errormessage -like "*Array cannot be null.*") {Write-Output "The SQL Selection contained no Data"} 
                }


                #Decode Data for test feedback
                Write-Output "Upload Data:"
                [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String("$encodedata"))


#Set XML Body
$body = @"
--$boundry
Content-Disposition: form-data; name="mergeUpload"
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<mergeUpload>
    <fileName>member.csv</fileName>
    <fileEncoding>UTF-8</fileEncoding>
    <separator>,</separator>
    <criteria>EMAIL</criteria>
    <dateFormat>yyyy-MM-dd HH24:mi:ss</dateFormat>
    <mapping>
        <column>
            <colNum>1</colNum>
            <fieldName>EMAIL</fieldName>
            <toReplace>true</toReplace>
        </column>
        <column>
            <colNum>2</colNum>
            <fieldName>SOURCEAPP</fieldName>
            <toReplace>true</toReplace>
        </column>
        <column>
            <colNum>3</colNum>
            <fieldName>DATEUNJOIN</fieldName>
            <toReplace>true</toReplace>
        </column>
    </mapping>
</mergeUpload>
--$boundry--
--$boundry
Content-Disposition: form-data; name="inputStream"; filename="member.csv"
Content-Type: application/octet-stream
Content-Transfer-Encoding: base64

$encodedata
--$boundry--
"@


                #API Post and capture Response
                Write-Output "$accountname`: Starting Data Upload"
                $uri = "https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/mergeUpload"

               TRY { [xml]$r=Invoke-RestMethod -uri $uri -body $body  -ContentType "multipart/form-data; boundary=$boundry" -Method Put}

               CATCH [System.Net.WebException] {
               
               $errormessage = $_.ErrorDetails
               $errortype = $_.Exception.GetType().FullName
               
               Write-Output "Error Message: $errormessage"
               Write-Output "Error Type: $errortype"
               #ToDo: must loop here untill potential 5 uploads are donw to allow it to proceed
               
               }


                $responseStatus = $r.response.responseStatus
                $uploadid = $r.response.result."#text"
                Write-Output "$accountname`: Upload submittion $responseStatus, UploadID: $uploadid"




$errorcount = $null
$filestatus = $null
#Loop Until Ready and then Download File - add time out at some point
Write-Output "$accountname`: Downloading File: $exportid Type: $type"
$timeout = New-TimeSpan -Minutes 20
$waittime = New-TimeSpan -Seconds 30
$waittime2 = New-TimeSpan -Minutes 3
$sw = [Diagnostics.Stopwatch]::StartNew()

while($filestatus -ne "DONE")
    {
        IF ($sw.Elapsed -gt $timeout)
            {
                $totalminutes = $Timeout.TotalMinutes
                Write-Output "$accountname`: The Upload for UploadID: $uploadid Timed out after $totalminutes Minutes. File Status: $filestatus"
                Write-Output "########## Upload Process END for $accountname ##########`n"

                #sql set status to time out and highlite yellow
                $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusDownload='Timeout' WHERE Account = '$accountname'"

                $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query

                #Exit the entire job and mark it as Timeout
                return
            }

            
    #ERROR: File validation has failed
    #FAILURE: Database import has failed
    # need to handle thease states


    #
    #

        IF ($sw.Elapsed -lt $waittime) {Start-Sleep -s 5} ELSEIF ($sw.Elapsed -lt $waittime2) {Start-Sleep -s 15} ELSE {Start-Sleep -s 30}

        TRY {
                [xml]$r=Invoke-RestMethod https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/$uploadId/getUploadStatus 
            }

        CATCH
            {
                $errorcount += 1
                $errortype = $_.Exception.GetType().FullName
                Write-Output "$accountname`: The error $_ occured"
                Write-Output "$accountname`: The error type is: $errortype"
                Write-Output "$accountname`: The Error count is: $errorcount"
                
                IF ($errorcount -gt 3) {return}

            }

        $filestatus = $r.response.uploadStatus.status

            IF ($filestatus -ne "DONE")
                {
                    $elapsed = $sw.Elapsed.TotalSeconds
                    Write-Output "$accountname`: Upload Pending ($elapsed Seconds Elapsed), Status: $filestatus, Retrying"
                }
    }#Download File End
$sw.Stop()
#ToDo: the loop seems to have an ocasional issue where it does not finish or report any error text but failes the job




Write-Output "$accountname`: File Upload Complete"



[xml]$r=Invoke-RestMethod https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/$uploadId/getLogFile

$uploadlog = $r.response.result."#text"
Write-Output "$accountname`: Log Downloaded"
#ToODO: parse this log and use the values for an inserted column on the report, either on a new column or to replace the uplocaded colums as it is partitialy redundt and will alwase show the same value accress all accounts



TRY {
        [xml]$r=Invoke-RestMethod https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/308579/getBadFile

        $badfile = $r.response.result."#text"
        $badfilecount = $badfile | Measure-Object -Line
        $failedrecords = $badfilecount.lines
        $badfile | Out-File -Append "Microsoft.PowerShell.Core\FileSystem::\\sql-api\API\Smart Focus\Logs\UnsubscribeSync\BadFile.log"
        Write-Output "Will download Bad file and count records for report, records: $failedrecords"
    }
    
CATCH [System.Net.WebException]
    {
        $badfileerror = $_.ErrorDetails.Message
        Write-Output "No bad file availible for download: $badfileerror"
        $failedrecords = 0 # to be used in report?
    }


   


    }#End Job
}#Loop End


get-job | Receive-Job 

#get-job | Remove-Job -force


#todo change the upload timer loop to me 30 seconds as the uploads will alwase take ages, perhaps have it speed up if the file status changes to running/processing as this stage is short compaired to qued


#todo: foir testing add a sql statment to delete all records from the sync table as soon as they arring where the record does not contain reactiv in the name and then use this to tedsst the entire process on all accountss repeditivley as well as the insert stationts into the production tabless

