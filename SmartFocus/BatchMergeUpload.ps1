﻿cls
#Load Config
$erroractionpreference = "Stop"
. "C:\API\Smart Focus\Config\REACTIV_A.ps1"
'Loaded Config'


#Open Session
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"
'Connection '+$r.response.responseStatus


#Create Boundry
$set    = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray()
$boundry = ""
for ($x = 0; $x -lt 22; $x++) {
    $boundry += $set | Get-Random
}
#return $boundry


#Set URI
$uri = "https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/mergeUpload"
       # https://{server}/apibatchmember/services/rest/batchmemberservice/{token}/batchmember/mergeUpload




#Set Test Data
$data = "clinton.edmondson2@reactiv.co.uk,Clinton,08/25/2015"


#Encode Data
$encode = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($data))


#Decode Data
$decode = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String("$datae"))




#Set XML Body
$body = @"
--$boundry
Content-Disposition: form-data; name="mergeUpload"
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<mergeUpload>
    <fileName>member.csv</fileName>
    <fileEncoding>UTF-8</fileEncoding>
    <separator>,</separator>
    <criteria>EMAIL</criteria>
    <dateFormat>mm/dd/yyyy</dateFormat>
    <mapping>
        <column>
            <colNum>1</colNum>
            <fieldName>EMAIL</fieldName>
            <toReplace>true</toReplace>
        </column>
        <column>
            <colNum>2</colNum>
            <fieldName>FIRSTNAME</fieldName>
            <toReplace>true</toReplace>
        </column>
        <column>
            <colNum>2</colNum>
            <fieldName>DATEUNJOIN</fieldName>
            <toReplace>true</toReplace>
        </column>
    </mapping>
</mergeUpload>
--$boundry--
--$boundry
Content-Disposition: form-data; name="inputStream"; filename="member.csv"
Content-Type: application/octet-stream
Content-Transfer-Encoding: base64

$encode
--$boundry--
"@


#API Post
[xml]$r=Invoke-RestMethod -uri $uri -body $body  -ContentType "multipart/form-data; boundary=$boundry" -Method Put


#Capture Response
$data = $r
$data.response.result."#text"


























#Close Session
[xml]$r=Invoke-RestMethod -Uri https://$server/apiccmd/services/rest/connect/close/$token
$r.response.result."#text"