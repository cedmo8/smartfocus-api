﻿#SQL Process Data
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = "EXEC SMARTFOCUS.dbo.SF_Unsubscription_ProcessData"
TRY {
        Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query -QueryTimeout 1 -ErrorAction Stop
    }


CATCH [Microsoft.SqlServer.Management.PowerShell.SqlPowerShellSqlExecutionException]
    {
        $errormessage = $_.exception.message
         Write-Output "$accountname`: The Data Processing Stored Procedure Failed"

        IF ($errormessage -like '*Timeout expired*')
            {
                Write-Output "$accountname`: The Data Processing Stored Procedure Timed Out"
                $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusDownload='Timeout' WHERE Account = 'PRODUCTION'"
                Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query
            }
    }


$sql



Display errors -OutputSqlErrors




    {
        Write-Output "the error:"

    $_.Exception.GetType().FullName
    }


$error.errors | Get-Member

try
{
    1/0
}
catch [DivideByZeroException]
{
    Write-Host "Divide by zero exception"
}
catch [System.Net.WebException],[System.Exception]
{
    Write-Host "Other exception"
}
finally
{
    Write-Host "cleaning up ..."
}