﻿cls
$ErrorActionPreference = 'stop'
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")  

$Form = New-Object System.Windows.Forms.Form    
$Form.Size = New-Object System.Drawing.Size(700,500)
$Form.Text = "Copy Creative"
$Form.StartPosition = "CenterScreen"

$Form.FormBorderStyle = 'Fixed3D'
$Form.MaximizeBox = $false






############################################## Start functions

$fromaccount = $null

function CopyMessageButton {
#$outputBox.text = $DropDownBox.text # use .text field to allow user entry

if ( $DropDownBox.SelectedItem -like '*:*' -and $objFromBox.SelectedItem -like '*REACTIV*' -and $objToBox.SelectedItem -like '*REACTIV*') {

$pbrTest.Value = 0  

#Set Vars
$messageid = $DropDownBox.SelectedItem.ToString()  -replace ":.*"
$fromaccount = $objFromBox.SelectedItem.ToString()
$toaccount = $objToBox.SelectedItem.ToString()
$outputBox.text = "Copying Message ID $messageid  from $fromaccount to $toaccount"

#temp
#$fromaccount = 'REACTIV_A'
#$messageid = '2175355'

#Load Config
$outputBox.text +="
Loading Config for $fromaccount"
try {. "C:\API\Smart Focus\Config\$fromaccount.ps1"}
catch {$outputBox.text = "Error loading Config for $fromaccount Check C:\API\Smart Focus\Config"
[System.Windows.Forms.Application]::DoEvents()
$updateMessageButton.enabled = $true
$Form.refresh()
return}
$pbrTest.Value = 5


#Open Session
$outputBox.text +="
Connecting to Server"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"
$outputBox.text +="
Connection "+$r.response.responseStatus
$pbrTest.Value = 15


#Get Message and Set/Report Name # have GUI button to get last messaged from selected account as well as manual input
$outputBox.text +="
Retreving Message Details"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/message/getMessage/$token/$messageid
$outputBox.text +='
Message to Copy: '+$r.response.message.name+'
From Line: '+$r.response.message.from+'
Subject: '+$r.response.message.subject+'
Date Created: '+$r.response.message.createDate
$clonename = '[API Clone] '+$r.response.message.name -replace '(.{50}).+','$1'
$pbrTest.Value = 25


#Duplicate Message to Copy
$outputBox.text +="
Cloning Message"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/message/cloneMessage/$token/$messageid/$clonename
$clonemessageid = $r.response.result."#text"
$outputBox.text +='
Duplicate Message: '+$r.response.responsestatus+'
Duplicate Message ID: '+$clonemessageid
$pbrTest.Value = 30


#Untrack Links from Duplicate
IF ($LinkTracking.Checked = $true) {
$outputBox.text +="
Untracking Links"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/message/untrackAllLinks/$token/$clonemessageid
$outputBox.text +='
Untracking Links: '+$r.response.responsestatus
}


#Get Source Message and save it to Vars
$outputBox.text +="
Exporting Message"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/message/getMessage/$token/$clonemessageid
$message = $r.response.message #truncate if longer than max name length
$outputBox.text +='
Export: '+$r.response.responsestatus+'
Exported Message Name: '+$r.response.message.name
$pbrTest.Value = 40

#Fields Retrived
$outputBox.text +="$(
#$message.body
$message.createdate
$message.encoding
$message.from
$message.fromEmail
$message.hotmailUnsubFlg
$message.id
$message.isBounceback
$message.name
$message.replyTo
$message.replyToEmail
$message.subject
$message.to
$message.type)"


#Delete Duplicate Message
$outputBox.text +="
Deleteing Clone"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/message/deleteMessage/$token/$clonemessageid
$outputBox.text +='
Delete Clone: '+$r.response.responsestatus
$pbrTest.Value = 45


#Count & Replace Null Links with &&&
IF ($LinkTracking.Checked = $true) {
$outputBox.text +="
Counting Blank Links"
$htmlcount = ([regex]::Matches($message.body, 'href=""' )).count
$textcount = ([regex]::Matches($message.body, "not to receive these emails please go to our unsubscribe page: `n" )).count
$outputBox.text +="
Blank Links Found -  HTML: $htmlcount TEXT: $textcount
Replacing HTML Blank Links"
$messagebody = $message.body -replace 'href=""', 'href="&&&"'
$messagebody = $message.body -replace "not to receive these emails please go to our unsubscribe page: `n", "not to receive these emails please go to our unsubscribe page: &&&`n"
$outputBox.text +="
Replaced $($htmlcount+$textcount) Links with &&&"
}




#410893
























$outputBox.text +="
End Script"





}#End IF

ELSE {$outputBox.text='Pick an account and or message'}# add else if to check iof it is the creative or account not selected or both


  
     } #end CopyMessageButton


function UpdateMessageButton {

if ( $objFromBox.SelectedItem -like '*REACTIV*' ){

$updateMessageButton.enabled = $false
$Form.refresh()

$pbrTest.Value = 0

$outputBox.text = 'Loading Creatives from '+$objFromBox.SelectedItem.ToString()

$DropDownBox.Items.Clear();

$fromaccount = $null
$fromaccount = $objFromBox.SelectedItem.ToString()


#Load Config
$outputBox.text +='
Loading Config for'+$fromaccount

try {. "C:\API\Smart Focus\Config\$fromaccount.ps1"}
catch
{
$outputBox.text = "Error loading Config for $fromaccount Check C:\API\Smart Focus\Config"
[System.Windows.Forms.Application]::DoEvents()
$updateMessageButton.enabled = $true
$Form.refresh()
return
}

$pbrTest.Value = 15


#Open Session
$outputBox.text +='
Connecting to Server'
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"
'Connection '+$r.response.responseStatus
$pbrTest.Value = 30


$outputBox.text +='
Building API Request'
[xml]$SOAP = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:api="http://api.service.apiccmd.emailvision.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <api:getMessageSummaryList>
         <token>'+$token+'</token>
         <listOptionsEntity>
            <page>1</page>
            <pageSize>10</pageSize>
            <sortOptions>
               <sortOption>
                  <column>createdDate</column>
                  <order>DESC</order>
               </sortOption>
            </sortOptions>
         </listOptionsEntity>
      </api:getMessageSummaryList>
   </soapenv:Body>
</soapenv:Envelope>'
$pbrTest.Value = 40


$outputBox.text +='
Submitting API Request'
$URI = "http://$server/apiccmd/services/CcmdService?wsdl"
[xml]$s = Invoke-WebRequest $uri -Method post -ContentType 'text/xml' -Body $SOAP -DisableKeepAlive -UseBasicParsing
$pbrTest.Value = 70

$outputBox.text +='
Processing Response'
$data = $s.envelope.body.getMessageSummaryListResponse.return.messageSummaryList.messageSummary | Select-Object name,messageid,subject,createdDate
$pbrTest.Value = 80

$outputBox.text +='
Loading Data'
$data | ForEach-Object {

$DropDownBox.Items.Add([string]$_.messageid+': '+[string]$_.name)

}#End Foreach
$DropDownBox.text = "Select Creative"
$pbrTest.Value = 90




#Close Session
$outputBox.text +='
Closing Connection'
[xml]$r=Invoke-RestMethod -Uri https://$server/apiccmd/services/rest/connect/close/$token
$pbrTest.Value = 100

$outputBox.text +='
Complete, Creatives Updated'

[System.Windows.Forms.Application]::DoEvents()
$updateMessageButton.enabled = $true
$Form.refresh()


                     }#End IF

ELSE {$outputBox.text='Pick an account to load the creatives from.'}



                     } #end UpdateMessageButton



                     <#


                     Get-Service | Select-Object Status, Name, DisplayName | ConvertTo-HTML | Out-File C:\Scripts\Test.htm

Invoke-Expression C:\Scripts\Test.htm


preview creative


replace tracking links by specifing what to rewrite and what to change it too




                     #>





function FindReplaceButton {


#Add-Type -AssemblyName System.Windows.Forms


$Form2.ShowDialog() 



}

$Form2 = New-Object system.Windows.Forms.Form



$DropDownBox2 = New-Object System.Windows.Forms.ComboBox
$DropDownBox2.Location = New-Object System.Drawing.Size(30,30) 
$DropDownBox2.Size = New-Object System.Drawing.Size(150,20)
$DropDownBox2.text = "Find and Replace"
$DropDownBox2.DropDownHeight = 200 
$Form2.Controls.Add($DropDownBox2)

############################################## end functions

############################################## Start drop down boxes

$Form.KeyPreview = $True
$Form.Add_KeyDown({if ($_.KeyCode -eq "Escape") 
    {$Form.Close()}})


# Init ProgressBar
$pbrTest = New-Object System.Windows.Forms.ProgressBar
$pbrTest.Maximum = 100
$pbrTest.Minimum = 0
$pbrTest.Location = new-object System.Drawing.Size(10,220)
$pbrTest.size = new-object System.Drawing.Size(565,20)
#$i = 0
$Form.Controls.Add($pbrTest)




$objFromLabel = New-Object System.Windows.Forms.Label
$objFromLabel.Location = New-Object System.Drawing.Size(10,10) 
$objFromLabel.Size = New-Object System.Drawing.Size(120,20) 
$objFromLabel.Text = "From Account:"
$Form.Controls.Add($objFromLabel) 


$objFromBox = New-Object System.Windows.Forms.ListBox 
$objFromBox.Location = New-Object System.Drawing.Size(10,30) 
$objFromBox.Size = New-Object System.Drawing.Size(120,20) 
$objFromBox.Height = 160

[void] $objFromBox.Items.Add("REACTIV_A")
[void] $objFromBox.Items.Add("REACTIV_B")
[void] $objFromBox.Items.Add("REACTIV_C")
[void] $objFromBox.Items.Add("REACTIV_D")
[void] $objFromBox.Items.Add("REACTIV_E")
[void] $objFromBox.Items.Add("REACTIV_F")
[void] $objFromBox.Items.Add("REACTIV_G")
[void] $objFromBox.Items.Add("REACTIV_NEW")
[void] $objFromBox.Items.Add("REACTIV_MED")
[void] $objFromBox.Items.Add("REACTIV_PRE")
[void] $objFromBox.Items.Add("REACTIV_WELCOME")
[void] $objFromBox.Items.Add("REACTIV_TWO")

$Form.Controls.Add($objFromBox) 



$objFromLabel = New-Object System.Windows.Forms.Label
$objFromLabel.Location = New-Object System.Drawing.Size(150,10) 
$objFromLabel.Size = New-Object System.Drawing.Size(120,20) 
$objFromLabel.Text = "To Account:"
$Form.Controls.Add($objFromLabel) 


$objToBox = New-Object System.Windows.Forms.ListBox 
$objToBox.Location = New-Object System.Drawing.Size(150,30) 
$objToBox.Size = New-Object System.Drawing.Size(120,20) 
$objToBox.Height = 160

[void] $objToBox.Items.Add("REACTIV_A")
[void] $objToBox.Items.Add("REACTIV_B")
[void] $objToBox.Items.Add("REACTIV_C")
[void] $objToBox.Items.Add("REACTIV_D")
[void] $objToBox.Items.Add("REACTIV_E")
[void] $objToBox.Items.Add("REACTIV_F")
[void] $objToBox.Items.Add("REACTIV_G")
[void] $objToBox.Items.Add("REACTIV_NEW")
[void] $objToBox.Items.Add("REACTIV_MED")
[void] $objToBox.Items.Add("REACTIV_PRE")
[void] $objToBox.Items.Add("REACTIV_WELCOME")
[void] $objToBox.Items.Add("REACTIV_TWO")

$Form.Controls.Add($objToBox) 



$DropDownBox = New-Object System.Windows.Forms.ComboBox
$DropDownBox.Location = New-Object System.Drawing.Size(280,30) 
$DropDownBox.Size = New-Object System.Drawing.Size(250,20)
$DropDownBox.text = "Update Messages"
$DropDownBox.DropDownHeight = 200 
$Form.Controls.Add($DropDownBox)



$SelectMessageLabel = New-Object System.Windows.Forms.Label
$SelectMessageLabel.Location = New-Object System.Drawing.Size(280,10) 
$SelectMessageLabel.Size = New-Object System.Drawing.Size(170,20) 
$SelectMessageLabel.Text = "Select Message or Enter ID:"
$Form.Controls.Add($SelectMessageLabel) 





#####Tests#####


$groupBox = New-Object System.Windows.Forms.GroupBox
$groupBox.Location = New-Object System.Drawing.Size(595,140) 
$groupBox.size = New-Object System.Drawing.Size(80,100) 
$groupBox.text = "Send Tests:" 
$Form.Controls.Add($groupBox) 

############################################## end group boxes

############################################## Start check boxes

$procName = New-Object System.Windows.Forms.checkbox
$procName.Location = New-Object System.Drawing.Size(10,20)
$procName.Size = New-Object System.Drawing.Size(60,20)
#$procName.Checked = $true
$procName.Text = "Clint"
$groupBox.Controls.Add($procName)

$procLoad = New-Object System.Windows.Forms.checkbox
$procLoad.Location = New-Object System.Drawing.Size(10,40)
$procLoad.Size = New-Object System.Drawing.Size(60,20)
$procLoad.Text = "Steve"
$groupBox.Controls.Add($procLoad)

$procSpeed = New-Object System.Windows.Forms.checkbox
$procSpeed.Location = New-Object System.Drawing.Size(10,60)
$procSpeed.Size = New-Object System.Drawing.Size(60,20)
$procSpeed.Text = "Tony"
$groupBox.Controls.Add($procSpeed)



$LinkTracking = New-Object System.Windows.Forms.checkbox
$LinkTracking.Location = New-Object System.Drawing.Size(450,200)
$LinkTracking.Size = New-Object System.Drawing.Size(150,20)
$LinkTracking.Checked = $true
$LinkTracking.Text = "Untrack/Retrack Links"
$Form.Controls.Add($LinkTracking)


$HTMLTest = New-Object System.Windows.Forms.checkbox
$HTMLTest.Location = New-Object System.Drawing.Size(285,90)
$HTMLTest.Size = New-Object System.Drawing.Size(55,20)
$HTMLTest.Checked = $true
$HTMLTest.enabled = $false
$HTMLTest.Text = "HTML"
$Form.Controls.Add($HTMLTest)

$TextTest = New-Object System.Windows.Forms.checkbox
$TextTest.Location = New-Object System.Drawing.Size(340,90)
$TextTest.Size = New-Object System.Drawing.Size(55,20)
$TextTest.Checked = $true
$TextTest.enabled = $false
$TextTest.Text = "Text"
$Form.Controls.Add($TextTest)



############################################## end drop down boxes

############################################## Start text fields

$outputBox = New-Object System.Windows.Forms.TextBox 
$outputBox.Location = New-Object System.Drawing.Size(10,250) 
$outputBox.Size = New-Object System.Drawing.Size(565,200) 
$outputBox.MultiLine = $True 
$outputBox.ScrollBars = "Vertical" 
$Form.Controls.Add($outputBox) 

############################################## end text fields

############################################## Start buttons

$Button = New-Object System.Windows.Forms.Button 
$Button.Location = New-Object System.Drawing.Size(560,30) 
$Button.Size = New-Object System.Drawing.Size(110,80) 
$Button.Text = "Copy" 
$Button.Add_Click({CopyMessageButton}) 
$Form.Controls.Add($Button)

$UpdateMessageButton = New-Object System.Windows.Forms.Button 
$UpdateMessageButton.Location = New-Object System.Drawing.Size(420,60) 
$UpdateMessageButton.Size = New-Object System.Drawing.Size(110,30) 
$UpdateMessageButton.Text = "Update Messages" 
$UpdateMessageButton.Add_Click({UpdateMessageButton}) 
$Form.Controls.Add($UpdateMessageButton)


$PreviewMessageButton = New-Object System.Windows.Forms.Button 
$PreviewMessageButton.Location = New-Object System.Drawing.Size(280,60) 
$PreviewMessageButton.Size = New-Object System.Drawing.Size(110,30) 
$PreviewMessageButton.Text = "Preview Message"
#$PreviewMessageButton.enabled = $false
$PreviewMessageButton.Add_Click({PreviewMessageButton}) 
$Form.Controls.Add($PreviewMessageButton)


$FindReplaceButton = New-Object System.Windows.Forms.Button 
$FindReplaceButton.Location = New-Object System.Drawing.Size(280,140) 
$FindReplaceButton.Size = New-Object System.Drawing.Size(110,30) 
$FindReplaceButton.Text = "Find and Replace"
#$FindReplaceButton.enabled = $false
$FindReplaceButton.Add_Click({FindReplaceButton}) 
$Form.Controls.Add($FindReplaceButton)



$BroswerForm = New-Object system.Windows.Forms.Form  
#$BroswerForm.Size = New-Object System.Drawing.Size(800,1000)

$BroswerForm.StartPosition = "CenterScreen"

#$BroswerForm.AutoScroll = $True
$BroswerForm.AutoSize = $True
$BroswerForm.AutoSizeMode = "GrowAndShrink"

#$BroswerForm.FormBorderStyle = 'Fixed3D'
$BroswerForm.MaximizeBox = $false

# Main Browser

$webBrowser1 = New-Object System.Windows.Forms.WebBrowser
$webBrowser1.IsWebBrowserContextMenuEnabled = $true

$webBrowser1.Width = 800
$webBrowser1.Height = 900
$webBrowser1.Location = "0, 0"
#$webBrowser1.AutoSize = $True
$BroswerForm.Controls.Add($webBrowser1)

 #$webBrowser1 | Get-Member

function PreviewMessageButton {

#Set Vars
$messageid = $DropDownBox.SelectedItem.ToString()  -replace ":.*"
$fromaccount = $objFromBox.SelectedItem.ToString()

#temp
#$fromaccount = 'REACTIV_A'
#$messageid = '2188667'

#Load Config
try {. "C:\API\Smart Focus\Config\$fromaccount.ps1"}
catch {$outputBox.text = "Error loading Config for $fromaccount Check C:\API\Smart Focus\Config"
[System.Windows.Forms.Application]::DoEvents()
return}




#Open Session

[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"

#Get message
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/message/getMessage/$token/$messageid



$previewname = $r.response.message.name
$previewbody = $r.response.message.body


$previewbody | Out-File "C:\API\Smart Focus\Creatives\$messageid.html"


#Close Session
[xml]$r=Invoke-RestMethod -Uri https://$server/apiccmd/services/rest/connect/close/$token
$BroswerForm.Text = "HTML Preview - $previewname"
$webBrowser1.URL = "C:\API\Smart Focus\Creatives\$messageid.html"
$BroswerForm.ShowDialog()

$BroswerForm.Refresh()
}


# Get Code Button, prompt for text or html
#option to export (checkbox to export createive and save to files with a hyperlink on wherer to open them)
#https://adminscache.wordpress.com/2013/05/22/open-web-browser-in-powershell-gui/


############################################## end buttons

$Form.Add_Shown({$Form.Activate()})
[void] $Form.ShowDialog()