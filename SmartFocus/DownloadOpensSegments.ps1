#Clean Varables
Remove-Variable * -ErrorAction SilentlyContinue

#Create Hash Table of Accounts and Segment
$accountlist = [ordered]@{
    'REACTIV_A'       = '1943406'
    'REACTIV_B'       = '1943137'
    'REACTIV_C'       = '1960367'
    'REACTIV_D'       = '1960419'
    'REACTIV_E'       = '407505'
#    'REACTIV_F'       = '6'
    'REACTIV_G'       = '411424'
    'REACTIV_NEW'     = '1990041'
#    'REACTIV_MED'     = '9'
#    'REACTIV_WELCOME' = '10'
#    'REACTIV_PRE'     = '8'
#    'REACTIV_TWO'     = '12'
    }

#Loop Throught Accounts
ForEach ($account in $accountlist.GetEnumerator()) {

#Start Job for Account
Start-Job -Name $account.name -ArgumentList $account.name,$account.value -ScriptBlock {

$accountname = $args[0]
$segmentid = $args[1]
Write-Output "`naccount: $accountname Segment: $segmentid"


#Load Config
$erroractionpreference = "Stop"
Import-Module sqlps -WarningAction SilentlyContinue
Write-Output "Loading Config for: $accountname"
. "C:\API\Smart Focus\Config\REACTIV_A.ps1"
Write-Output "Config Loaded"

#Open Session
Write-Output "Connecting to $accountname"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"
Write-Output "Connection $($r.response.responseStatus)"


#Close Session
Write-Output "Closing Session $accountname"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/close/$token
Write-Output "$accountname $($r.response.result."#text")`n`n"
}}


#Retreve Results fron Job
$results = $null
Write-Output "Waiting for Jobs"
get-job | Wait-Job | Receive-Job | ForEach-Object {
    $results +=  "$_`n"
    #Remove-Job $_ -Force
}

Get-job | remove-job

$results


$r.response.result

Write-Host "https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key"



$test = $null

$test += "test data`n"

$test



Receive-Job REACTIV_A -Keep



    
    Wait-Job -

    Remove-Job REACTIV_A
    Remove-Job REACTIV_B

    Get-job | remove-job

Receive-Job 64 -Keep

Remove-Job 


#Load Config
#$erroractionpreference = "Stop"
#Import-Module sqlps -WarningAction SilentlyContinue
#. "C:\API\Smart Focus\Config\$_.ps1"

#Open Session
#"Connecting to Server "+$_+"seccuss"
#[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
#$token = $r.response.result."#text"
#"Connection "+$r.response.responseStatus


}}





Get-job -IncludeChildJob

Receive-Job REACTIV_A -Keep

USE [SMARTFOCUS]
GO

INSERT INTO [dbo].[SF_Unsubscription_Sync]
           ([Email]
           ,[SourceApp]
           ,[DateUnjoin]
           ,[Origin])
     VALUES
           ('Clinton.Edmondson@reactiv.co.uk'
           ,'1'
           ,'2015/09/10'
           ,'Test')
GO








#Load Config
$erroractionpreference = "Stop"
Import-Module sqlps -WarningAction SilentlyContinue
. "C:\API\Smart Focus\Config\REACTIV_A.ps1"


#Open Session
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"







#Insert Data into SQL Table
$data = convertfrom-csv -inputobject $r.response.apiDownloadFile.fileContent -delimiter ","




$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'


$query = @"
INSERT INTO [dbo].[SF_Unsubscription_Sync]
           ([Email]
           ,[SourceApp]
           ,[DateUnjoin]
           ,[Origin])
     VALUES
           ('Clinton.Edmondson@reactiv.co.uk'
           ,'1'
           ,'2015/09/10'
           ,'REACTIV_A')
"@

$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query





$database = 'API'
$sqlserver = '.'
$table = 'dbo.SF_Wills_Opens_Import'

$data | ForEach-Object {

  $EMAIL = $_.EMAIL
  $CAMPAGNE_ID = $_.CAMPAGNE_ID

Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query "insert into $table VALUES ('$CAMPAGNE_ID','$EMAIL','Wills')"
}#Insert Data End



#Close Session
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/close/$token
$r.response.result."#text"
