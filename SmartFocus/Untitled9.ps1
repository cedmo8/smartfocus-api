﻿
Start-Transcript "c:\powershell\transcript"

#Set Test Data
#$data = "clinton.edmondson2@reactiv.co.uk,Clinton,08/25/2015"

$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'


$query = 'SELECT TOP 5 Email,SourceApp,CONVERT(VARCHAR(19), DateUnjoin, 120) FROM SMARTFOCUS.[dbo].[SF_Unsubscription_Sync]' #does this need nvarchar herer or could i just convert to another datetime?
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query 



$sql = $sql | ConvertTo-Csv -NoTypeInformation | Select-Object -Skip 1

$sql = $sql -replace '"'

#$sql = $sql -replace ,"a"

$OFS = "`r`n"

[string]$sql


#Encode Data
$encodedata = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($sql))


#Decode Data
[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String("$encodedata"))


Stop-Transcript

cls

$error