﻿#Load Config
$erroractionpreference = "Stop"
. "C:\API\Smart Focus\Config\REACTIV_A.ps1"
'Loaded Config'


#Open Session
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"
'Connection '+$r.response.responseStatus


#Create Boundry
$set = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray()
[string]$boundry = $null
for ($x = 0; $x -lt 22; $x++) {
    $boundry += $set | Get-Random
}


$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = @"
SELECT TOP 15
    Email,SourceApp,CONVERT(VARCHAR(19), DATEADD(HH,+1,DateUnjoin), 120) 
    FROM SMARTFOCUS.[dbo].[SF_Unsubscription_Sync]
    WHERE email like '%reactiv%'

"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query | ConvertTo-Csv -NoTypeInformation | Select-Object -Skip 1
#$sqlbackup = $sql


#http://stackoverflow.com/questions/4431706/retaining-newlines-in-powershell-string
#Set Output field separator line break insted of space (used in string concatination for encoding)
$OFS = "`r`n"

#Encode Data
TRY {
        $encodedata = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($sql))
    }
#ToDo: the encode statment failes if there is no data so need to add a catch for "Exception calling "GetBytes" with "1" argument(s): "Array cannot be null."

CATCH [System.Management.Automation.MethodInvocationException]
    {
        $errormessage = $_.exception.message
        Write-Output "Data encoding Failed with the Error: $errormessage"
        IF ($errormessage -like "*Array cannot be null.*") {Write-Output "The SQL Selection contained no Data"} 
    }


<#Exception calling "GetBytes" with "1" argument(s): "Array cannot be null.
Parameter name: chars"
At C:\Users\Clinton.Edmondson\OneDrive\Projects\Powershell\API\SmartFocus\UploadUnsubSegments.ps1:54 char:1
+ $encodedata = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetB ...
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:) [], ParentContainsErrorRecordException
    + FullyQualifiedErrorId : ArgumentNullException#>


#Decode Data for test feedback
[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String("$encodedata"))




#Set XML Body
$body = @"
--$boundry
Content-Disposition: form-data; name="mergeUpload"
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<mergeUpload>
    <fileName>member.csv</fileName>
    <fileEncoding>UTF-8</fileEncoding>
    <separator>,</separator>
    <criteria>EMAIL</criteria>
    <dateFormat>yyyy-MM-dd HH24:mi:ss</dateFormat>
    <mapping>
        <column>
            <colNum>1</colNum>
            <fieldName>EMAIL</fieldName>
            <toReplace>true</toReplace>
        </column>
        <column>
            <colNum>2</colNum>
            <fieldName>SOURCEAPP</fieldName>
            <toReplace>true</toReplace>
        </column>
        <column>
            <colNum>3</colNum>
            <fieldName>DATEUNJOIN</fieldName>
            <toReplace>true</toReplace>
        </column>
    </mapping>
</mergeUpload>
--$boundry--
--$boundry
Content-Disposition: form-data; name="inputStream"; filename="member.csv"
Content-Type: application/octet-stream
Content-Transfer-Encoding: base64

$encodedata
--$boundry--
"@


#API Post and capture 
$uri = "https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/mergeUpload"
[xml]$r=Invoke-RestMethod -uri $uri -body $body  -ContentType "multipart/form-data; boundary=$boundry" -Method Put


#Capture Response
$uploadid = $r.response.result."#text"
$uploadid










