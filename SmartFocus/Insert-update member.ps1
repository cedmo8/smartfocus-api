﻿#Load Config
$erroractionpreference = "Stop"
. "C:\API\Smart Focus\Config\REACTIV_A.ps1"


#Open Session
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token = $r.response.result."#text"
'Connection '+$r.response.responseStatus


#Set Headers
$headers = $null
$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
#$headers.Add("content-type", "application/xml")
$headers.Add("accept", "application/xml")


#Set URI
$uri = "https://$server/apimember/services/rest/member/insertOrUpdateMember/$token"


[xml]$body = @"
<?xml version="1.0" encoding="utf-8"?>
<synchroMember>
  <memberUID>EMAIL:john.bell4@reactivgroup.co.uk</memberUID>
  <dynContent>
    <entry>
      <key>SOURCEAPP</key>
      <value>5</value>
    </entry>
    <entry>
      <key>DATEUNJOIN</key>
      <value>08/27/2015</value>
    </entry>
  </dynContent>
</synchroMember>
"@



 



[xml]$r=Invoke-RestMethod $uri -body $body  -ContentType application/xml -Method Post

$data = $r

$data.response.result."#text"


#Close Session
[xml]$r=Invoke-RestMethod -Uri https://$server/apiccmd/services/rest/connect/close/$token
$r.response.result."#text"