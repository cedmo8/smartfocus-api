#Clean Varables
Remove-Variable * -ErrorAction SilentlyContinue
$erroractionpreference = "Continue"
$error.Clear()

#Set Logs
$startdate = Get-Date
$logdate = (Get-Date).ToString('yyyyMMdd')
$logfile = "\\Sql-api\api\Smart Focus\Logs\UnsubscribeSync\Sync_$logdate.Log"


#Reset Report
#ToDO: set startdate for production start time stamp, and write it to the database at teh end of the powershel script aafter conformation that there where no errors and the data has been uploaded
#--DECLARE @ProcessStartDate datetime
#--SET @ProcessStartDate = GETDATE()
#decided it is better to do in SQl SP as it better mirrors the way the rest of the accoutns report their last run date
#later decided that thisMUST be in the poeershell script incase the powershell script fails AFTER the SQL process is complete as the next run wiould not otherwise retroactivly process data



#ToDo: add runtime into process to show how long an accoutn takes to finish its process and into the production process as well.


$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = @"
UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report
SET
        UnsubDownload='0',
        BounceDownload='0',
        StatusDownload='Error',
        UnsubUpload='0',
        BounceUpload='0',
        StatusUpload='Error',
        CountStart='0',
        CountEnd='0',
        CountDiff='0'
GO

-- Temp reset of the Sync Table before process runs
Truncate table [dbo].[SF_Unsubscription_Sync]
GO
"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query

#Create Hash Table of Accounts and Segment
$accountlist = [ordered]@{
    'REACTIV_A'       = '1943415'
    #'REACTIV_B'       = '1943345'
    #'REACTIV_C'       = '1960364'
    #'REACTIV_D'       = '1960414'
    #'REACTIV_E'       = '407483'
    #'REACTIV_F'       = '328435'
    #'REACTIV_G'       = '411414'
    #'REACTIV_NEW'     = '1990039'
    #'REACTIV_MED'     = '1113670230','1113671069'
    #'REACTIV_WELCOME' = '1991232','1991237'
    #'REACTIV_PRE'     = '3507798','3508081'
    #'REACTIV_TWO'     = '3513781','3513782'
    }


#Loop Throught Accounts
ForEach ($account in $accountlist.GetEnumerator()) {

#Set Trigger or Standard Account
IF ($account.Value -is [system.array]) {$accounttype = 'trigger'} ELSE {$accounttype = 'standard'}


########################
### Download Process ###
########################


#Start Job for Account
Start-Job -Name $account.name -ArgumentList $account.name,$account.value,$accounttype -ScriptBlock {

#Collect Varables for Job
$accountname = $args[0]
$segmentid =   $args[1]
$accounttype = $args[2]
$erroractionpreference = "Stop"
#Import-Module sqlps -WarningAction SilentlyContinue

Write-Output "`n########## Download Process START for $accountname ##########"
Write-Output "Account Name: $accountname"
Write-Output "$accountname`: Account Type: $accounttype"


IF ($accounttype -eq 'trigger') {

        $unsubid = $segmentid[0]
        $bounceid = $segmentid[1]

        Write-Output "$accountname`: Unsub Segment ID: $unsubid"
        Write-Output "$accountname`: Bounce Segment ID: $bounceid"

    } ELSE {
        Write-Output "$accountname`: Segment ID: $segmentid"
}


#Load Account Configuration
Write-Output "$accountname`: Loading Configuration File"
try {. "C:\API\Smart Focus\Config\$accountname.ps1"}
catch {
    Write-Output "$accountname`: Error loading Configuration for $accountname Check C:\API\Smart Focus\Config"
    Write-Output "########## Download Process END for $accountname ##########`n"
    #sql set status to Config Error
    $sqlserver = 'SQL-API'
    $database = 'SMARTFOCUS'
    $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusDownload='Config' WHERE Account = '$accountname'"

    $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query
    return
    }
    Write-Output "$accountname`: Configuration Loaded Seccussfuly Login: $login"


#Open Session
Write-Output "$accountname`: Connecting to Account"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
$token  = $r.response.result."#text"
$responseStatus = $r.response.responseStatus
$tokenreport = $token -replace '(.{4}).+','$1'
Write-Output "$accountname`: Connection $responseStatus, Token: $tokenreport"

#ToDo: capture connection response and throw/return with error if not seccus or is failure - maby make catches for all API calls

#Process mutiple segments if account is a Trigger Account
IF ($accounttype -eq 'trigger') {



    #Load Unsub Segment and Display Name
    Write-Output "$accountname`: Loading Unsub Segment Name for ID: $unsubid"
    [xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/segmentationservice/$token/segment/$unsubid
    $segmentname = $r.response.segmentation.name
    Write-Output "$accountname`: Unsub Segment Name: $segmentname"

    #Load Bounce Segment and Display Name
    Write-Output "$accountname`: Loading Bounce Segment Name for ID: $bounceid"
    [xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/segmentationservice/$token/segment/$bounceid
    $segmentname = $r.response.segmentation.name
    Write-Output "$accountname`: Bounce Segment Name: $segmentname"

} ELSE {

    #Load Segment and Display Name
    Write-Output "$accountname`: Loading Segment Name for ID: $segmentid"
    [xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/segmentationservice/$token/segment/$segmentid
    $segmentname = $r.response.segmentation.name
    Write-Output "$accountname`: Segment Name: $segmentname"

}


foreach ($type in 'UNJOIN_MEMBERS','QUARANTINED_MEMBERS') {

#Set details for Unsubs loop
IF ($type -eq 'UNJOIN_MEMBERS'){
    $typename = 'Unsub'
    $sourceapp = 1
    Write-Output "$accountname`: Processing: $type"

        IF ($accounttype -eq 'trigger') {$segmentid = $unsubid}
}

#Set details for Bounce loop
IF ($type -eq 'QUARANTINED_MEMBERS'){
    $typename = 'Bounce'
    $sourceapp = 2
    Write-Output "$accountname`: Processing: $type"

        IF ($accounttype -eq 'trigger') {$segmentid = $bounceid}
}


#Create Download and set Export ID
Write-Output "$accountname`: Creating Download for SegmentID: $segmentid Type: $type"
[xml]$r=Invoke-RestMethod https://$server/apiexport/services/rest/createDownloadByMailinglist/$token/$segmentid/$type/EMAIL/TRUE/EMAIL/TRUE/CSV/?dateFormat=MM/dd/yyyy
$responseStatus = $r.response.responseStatus
$exportid = $r.response.result."#text"
Write-Output "$accountname`: Creation $responseStatus, ExportID: $exportid"

$errorcount = $null
$filestatus = $null
#Loop Until Ready and then Download File - add time out at some point
Write-Output "$accountname`: Downloading File: $exportid Type: $type"
$timeout = New-TimeSpan -Minutes 20
$waittime = New-TimeSpan -Seconds 30
$waittime2 = New-TimeSpan -Minutes 3
$sw = [Diagnostics.Stopwatch]::StartNew()

while($filestatus -ne "OK")
    {
        IF ($sw.Elapsed -gt $timeout) 
            {
                $totalminutes = $Timeout.TotalMinutes
                Write-Output "$accountname`: The download for ExportID: $exportid Type: $type Timed out after $totalminutes Minutes. File Status: $filestatus"
                Write-Output "########## Download Process END for $accountname ##########`n"

                #sql set status to time out and highlite yellow
                $sqlserver = 'SQL-API'
                $database = 'SMARTFOCUS'
                $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusDownload='Timeout' WHERE Account = '$accountname'"

                $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query

                #Exit the entire job and mark it as Timeout
                return
            }

        IF ($sw.Elapsed -lt $waittime) {Start-Sleep -s 5} ELSEIF ($sw.Elapsed -lt $waittime2) {Start-Sleep -s 15} ELSE {Start-Sleep -s 30}

        TRY {
                [xml]$r=Invoke-RestMethod https://$server/apiexport/services/rest/getDownloadFile/$token/$exportid
            }

        CATCH
            {
                $errorcount += 1
                $errortype = $_.Exception.GetType().FullName
                Write-Output "$accountname`: The error $_ occured"
                Write-Output "$accountname`: The error type is: $errortype"
                Write-Output "$accountname`: The Error count is: $errorcount"
                
                IF ($errorcount -gt 3) {return}

            }

        $filestatus = $r.response.apiDownloadFile.filestatus

            IF ($filestatus -ne "OK")
                {
                    $elapsed = $sw.Elapsed.TotalSeconds
                    Write-Output "$accountname`: $type File Not Ready ($elapsed Seconds Elapsed), Retrying"
                }
    }#Download File End
$sw.Stop()
#ToDo: the loop seems to have an ocasional issue where it does not finish or report any error text but failes the job

#Convert Data to CSV and Report Record Count
$download = convertfrom-csv -inputobject $r.response.apiDownloadFile.fileContent -delimiter ","
$elapsed = $sw.Elapsed.TotalSeconds
$recordcount = $download.count
Write-Output "$accountname`: $type File Download Complete."
Write-Output "$accountname`: Time Elapsed: $elapsed Seconds"
Write-Output "$accountname`: $type Record Count: $recordcount"


#Insert Data into SQL Table
$date = Get-Date
#ToDO: Date in the sync and download history table do not have any time details, add them here to alow more finite tracking of record history
#ToDO: look at changing the DateJunoin column in the syncetable to DownloadDate to be factualy accurate


$download | ForEach-Object {

#SQL Insert
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$email = $_.EMAIL -replace "'"
$query = @"
INSERT INTO SMARTFOCUS.dbo.SF_Unsubscription_Sync
           ([Email]
           ,[SourceApp]
           ,[DateUnjoin]
           ,[Origin])
     VALUES
           ('$email'
           ,'$sourceapp'
           ,'$date'
           ,'$accountname')
"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query


}#Insert Data End

#ToDo: set SQL server connection Vars at head of job as all are using the same database and do not need to be repetedaly declared
#ToDO: Look at standidising or removing the Hearstrings for the smaller SQL querys as they are not needed
#Update Report with Download Statistics
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = @"
UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET 
	       $typename`Download='$recordcount'
WHERE Account = '$accountname'
GO
"@
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query

#Mark Account Complete if success on the HardBounce Export
IF ($typename -eq 'Bounce') {

    #Update Report with Download Statistics
    $date = Get-Date
    $sqlserver = 'SQL-API'
    $database = 'SMARTFOCUS'
    $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusDownload='Complete', LastRunDate='$date' WHERE Account = '$accountname'"
    $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query 
}

}#End UNSUB/HB Loop


#Close Session
Write-Output "$accountname`: Closing Session"
[xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/close/$token
$responseStatus = $r.response.result."#text"
Write-Output "$accountname`: $responseStatus"
#IF ($error -ne $null) {Write-Output "$accountname Errors: $error"}
Write-Output "########## Download Process END for $accountname ##########`n"


}}#END of JOB


####################
### Data Process ###
####################


#Retreve Results fron Job
#ToDo: look at  the -wait switch on the Receive-Job Cmdlet as a posible replacment for the wait-job one
$results = $null
Write-Output "`nWaiting for Jobs...  (Timeout in 35 Minutes)"
Get-Job | Wait-Job | Receive-Job | ForEach-Object {
    
    $results +=  "$_`n"
}


#SQL Process Data
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = "EXEC SMARTFOCUS.dbo.SF_Unsubscription_ProcessData"
TRY {
        $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query -QueryTimeout 600 -ErrorAction Stop
    }

CATCH [Microsoft.SqlServer.Management.PowerShell.SqlPowerShellSqlExecutionException]
    {
        $errormessage = $_.exception.message
         Write-Output "$accountname`: The Data Processing Stored Procedure Failed"

        IF ($errormessage -like '*Timeout expired*')
            {
                Write-Output "$accountname`: The Data Processing Stored Procedure Timed Out"
                $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusDownload='Timeout' WHERE Account = 'PRODUCTION'"
                $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query
            }
    }
#ToDo: Add try catch herer for "$sql = Invoke-Sqlcmd : Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding"


#report results test and clear Jobs
$results
Get-Job
Get-Job | Remove-Job -Force


######################
### Report Process ###
######################


#Log Job Results to File
#ToDo: look at adding the job loging to file to within thwe job to aloow for log creation befor the jobs finish, problems will be asyncronol looging in the file so will need to iover come perhaps with sepreat log files

#https://social.technet.microsoft.com/forums/windowsserver/en-US/3ea404aa-74d5-4c73-b6b7-8707bc560e23/invokesqlcmd-is-messing-with-my-providers

#Log Start
Write-Output "######################################`n### Log Start: $startdate ###`n######################################`n" |
Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$logfile"

#Log Download Content
Write-Output "########################`n### Download Process ###`n########################" | Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$logfile"
$results | Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$logfile"


#SQL Download Report - Change to update - maby better as a hast table if can be made globaley
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = "EXEC SMARTFOCUS.dbo.SF_Unsubscription_SendReport @logfile = '$logfile'"
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query


#Commit Production Start Date to Database
#ToDo: Put validation befor allowing this to run that ensures tha that there where no errors or timeouts
$sqlserver = 'SQL-API'
$database = 'SMARTFOCUS'
$query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET LastRunDate='$startdate' WHERE Account = 'PRODUCTION'"
$sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query


######################
### Upload Process ###
######################


Write-Output "######################`n### Upload Process ###`n######################" |
Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$logfile"


#Loop Throught Accounts
ForEach ($account in $accountlist.GetEnumerator()) {


    #Start Job for Account
    Start-Job -Name $account.name -ArgumentList $account.name -ScriptBlock {

        #Collect Varables for Job
        $accountname = $args[0]
        $erroractionpreference = "Stop"

        $sqlserver = 'SQL-API'
        $database = 'SMARTFOCUS'


        #$accountname = 'REACTIV_A'


        Write-Output "########## Upload Process START for $accountname ##########"
        Write-Output "Account Name: $accountname"


        #Load Account Configuration
        Write-Output "$accountname`: Loading Configuration File"

        TRY {
                . "C:\API\Smart Focus\Config\$accountname.ps1"
            }

        CATCH
            {
                Write-Output "$accountname`: Error loading Configuration for $accountname Check C:\API\Smart Focus\Config"
                Write-Output "########## Upload Process END for $accountname ##########`n"
                #sql set status to Config Error
                $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusUpload='Config' WHERE Account = '$accountname'"

                $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query
                return
            }


        Write-Output "$accountname`: Configuration Loaded Seccussfuly Login: $login"


        #Open Session
        Write-Output "$accountname`: Connecting to Account"
        [xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/open/$login/$pwd/$key
        $token  = $r.response.result."#text"
        $responseStatus = $r.response.responseStatus
        $tokenreport = $token -replace '(.{4}).+','$1'
        Write-Output "$accountname`: Connection $responseStatus, Token: $tokenreport"

        #ToDo: capture connection response and throw/return with error if not seccus or is failure - maby make catches for all API calls


        #Create Boundry
        Write-Output "$accountname`: Creating Boundry"
        $set = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray()
        [string]$boundry = $null
        for ($x = 0; $x -lt 22; $x++) {
            $boundry += $set | Get-Random
        }
        Write-Output "$accountname`: Boundry: $boundry"


        $query = @"
        SELECT TOP 15
            Email,SourceApp,CONVERT(VARCHAR(19), DATEADD(HH,+1,DateUnjoin), 120) 
            FROM SMARTFOCUS.[dbo].[SF_Unsubscription_Sync]
            WHERE email like '%reactiv%'
"@

        Write-Output "$accountname`: Selecting SQL data from Sync Table"
        $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query | ConvertTo-Csv -NoTypeInformation | Select-Object -Skip 1
        $sqlcount = $sql.count
        Write-Output "$accountname`: SQL Record Count: $sqlcount"


        #Set Output field separator line break insted of space (used in string concatination for encoding)
        $OFS = "`r`n"

        #Encode Data
        Write-Output "$accountname`: Encoding SQL Data in BASE64"
        TRY {
                $encodedata = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($sql))
                $datalength = $encodedata.Length
                Write-Output "$accountname`: Data Encoded length: $datalength"
            }


        #ToDo: the encode statment failes if there is no data so need to add a catch for "Exception calling "GetBytes" with "1" argument(s): "Array cannot be null."
        CATCH [System.Management.Automation.MethodInvocationException]
            {
                $errormessage = $_.exception.message
                Write-Output "Data encoding Failed with the Error: $errormessage"
                IF ($errormessage -like "*Array cannot be null.*") {Write-Output "The SQL Selection contained no Data"} 
            }


        #Decode Data for feedback
        $datasample = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String("$encodedata")) -split "`n" | Select-Object -First 4
        Write-Output "$accountname`: Decoded Upload Data Sample:`n#### Data Sample Start ####`n$datasample`n##### Data Sample End #####"
        



#Set XML Body
$body = @"
--$boundry
Content-Disposition: form-data; name="mergeUpload"
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<mergeUpload>
    <fileName>member.csv</fileName>
    <fileEncoding>UTF-8</fileEncoding>
    <separator>,</separator>
    <criteria>EMAIL</criteria>
    <dateFormat>yyyy-MM-dd HH24:mi:ss</dateFormat>
    <mapping>
        <column>
            <colNum>1</colNum>
            <fieldName>EMAIL</fieldName>
            <toReplace>true</toReplace>
        </column>
        <column>
            <colNum>2</colNum>
            <fieldName>SOURCEAPP</fieldName>
            <toReplace>true</toReplace>
        </column>
        <column>
            <colNum>3</colNum>
            <fieldName>DATEUNJOIN</fieldName>
            <toReplace>true</toReplace>
        </column>
    </mapping>
</mergeUpload>
--$boundry--
--$boundry
Content-Disposition: form-data; name="inputStream"; filename="member.csv"
Content-Type: application/octet-stream
Content-Transfer-Encoding: base64

$encodedata
--$boundry--
"@


        #API Post and capture Response
        Write-Output "$accountname`: Starting Data Upload"
        $uri = "https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/mergeUpload"

        TRY {
            [xml]$r=Invoke-RestMethod -uri $uri -body $body  -ContentType "multipart/form-data; boundary=$boundry" -Method Put
            $responseStatus = $r.response.responseStatus
            $uploadid = $r.response.result."#text"
            Write-Output "$accountname`: Upload Submittion $responseStatus, UploadID: $uploadid"
        }

        CATCH [System.Net.WebException] {
               
            Write-Output "$accountname`: Upload Submittion Failed"
            $errormessage = $_.ErrorDetails
            $errortype = $_.Exception.GetType().FullName
               
            Write-Output "Error Message: $errormessage"
            Write-Output "Error Type: $errortype"
            #ToDo: must loop here untill potential 5 uploads are donw to allow it to proceed or use the get upload slots methord
            return
        }


        $errorcount = $null
        $filestatus = $null
        Write-Output "$accountname`: Uploading File File: $uploadid datalength: $datalength"
        $timeout = New-TimeSpan -Minutes 20
        $sw = [Diagnostics.Stopwatch]::StartNew()

        # Upload Loop
        while($filestatus -notin "DONE","DONE WITH ERROR(S)") {
                    
            IF ($sw.Elapsed -gt $timeout) {

                    $totalminutes = $Timeout.TotalMinutes
                    Write-Output "$accountname`: The Upload for UploadID: $uploadid Timed out after $totalminutes Minutes. File Status: $filestatus"
                    Write-Output "########## Upload Process END for $accountname ##########`n"

                    #sql set status to time out and highlite yellow
                    $query = "UPDATE SMARTFOCUS.dbo.SF_Unsubscription_Report SET StatusUpload='Timeout' WHERE Account = '$accountname'"

                    $sql = Invoke-Sqlcmd -Database $database -ServerInstance $sqlserver -Query $query

                    #Exit the entire job and mark it as Timeout
                    return
            }


            # Account for the posibility of errors in the upload
            IF ($filestatus -in "ERROR","FAILURE"){

                Write-Output "$accountname`: The Upload for UploadID: $uploadid Failed after $totalminutes Minutes. File Status: $filestatus"
                #ToDo: posibily add a finction to save the error log when this statment is true
            }


            Start-Sleep -s 5


            TRY {
                
                #Get Upload Status
                [xml]$r=Invoke-RestMethod https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/$uploadid/getUploadStatus 
            }

            CATCH {

                #Report potential errors from the API web get request
                $errorcount += 1
                $errortype = $_.Exception.GetType().FullName
                Write-Output "$accountname`: The error $_ occured"
                Write-Output "$accountname`: The error type is: $errortype"
                Write-Output "$accountname`: The Error count is: $errorcount"
                
                IF ($errorcount -gt 3) {return}
            }

            $filestatus = $r.response.uploadStatus.status

            IF ($filestatus -notin "DONE","DONE WITH ERROR(S)") {

                $elapsed = $sw.Elapsed.TotalSeconds
                Write-Output "$accountname`: Upload Pending ($elapsed Seconds Elapsed), Status: $filestatus, Retrying"
            }
        }#Upload While End
        $sw.Stop()
        #ToDo: the loop seems to have an ocasional issue where it does not finish or report any error text but failes the job


        Write-Output "$accountname`: File Upload Complete"


        [xml]$r=Invoke-RestMethod https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/$uploadId/getLogFile

        $uploadlog = $r.response.result."#text"
        Write-Output "$accountname`: The Import Log has been Downloaded"
        #ToODO: parse this log and use the values for an inserted column on the report, either on a new column or to replace the uplocaded colums as it is partitialy redundt and will alwase show the same value accress all accounts


        TRY {
            [xml]$r=Invoke-RestMethod https://$server/apibatchmember/services/rest/batchmemberservice/$token/batchmember/$uploadid/getBadFile

            $badfile = $r.response.result."#text"
            $badfilecount = $badfile | Measure-Object -Line
            $failedrecords = $badfilecount.lines
            $badfile | Out-File -Append "Microsoft.PowerShell.Core\FileSystem::\\sql-api\API\Smart Focus\Logs\UnsubscribeSync\BadFile.log"
            Write-Output "$accountname`: There where $failedrecords Failed Records. This data has been saved into the log folder"
            }
    
        CATCH [System.Net.WebException]
            {
                # Catch the absence of a badfile
                $badfileerror = $_.ErrorDetails.Message
                Write-Output "No bad file availible for download: $badfileerror"
                $failedrecords = 0 # to be used in report? otherwise not needed
            }


        #Close Session
        Write-Output "$accountname`: Closing Session"
        [xml]$r=Invoke-RestMethod https://$server/apiccmd/services/rest/connect/close/$token
        $responseStatus = $r.response.result."#text"
        Write-Output "$accountname`: $responseStatus"
        Write-Output "########## Upload Process END for $accountname ##########`n"

    }#End Job
}#Loop End


$results = $null
Get-Job | Receive-Job -Wait | ForEach-Object {
    
    $results +=  "$_`n"
}

# Log Content
$results | Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$logfile"

Get-Job | Remove-Job -force


#todo: foir testing add a sql statment to delete all records from the sync table as soon as they arring where the record does not contain reactiv in the name and then use this to tedsst the entire process on all accountss repeditivley as well as the insert stationts into the production tabless


#Log Errors
IF ($error -ne $null) {"`n##############`n### Errors ###`n##############`n`n$error" |
Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$logfile"}


#Log End
$enddate = Get-Date
Write-Output "`n######################################`n#### Log End: $enddate ####`n######################################`n`n" |
Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$logfile"

















#TODO: make detailed Documentation for this process and host on Twiki
#ToDo: add documentation button or link in error explanation
#ToDo: Create visio document sshoecasing how the processs workss including the mutiple threads for downloads and uploads, and smaller details such as "count number of records in download andd write to log and read segment name and id and write to log
#ToDo: have subject line change to corrispond to the warning state e.g. (Error)"
#ToDo: Documentation should include casveat about how anythidng that john unsubscribes will not be downloaded by this process as he marked the records with a 1 meaning that they are alredy held localy
#ToDo: Create a History table of all records downloaded brefore dedupe and a history of all records uploaded after dedupe include date for both
#ToDo: have emv return the logs from the file upload/import and store thease on the api server  with a link to them in the report
#ToDo: have the report contain a list of failed uploads such as syntax errors ect.
#ToDo: find a way to allow the process to remember the date and time it left off so i can be run agaijn and again withour performing dupicate work, manly an issue for selecting midas data to upload as accounts are marken with a 1/2, perhaps use this marking methord in the unsub table or use the history table to dedupe the selection againsa however that does not cover the event that an account times out anas it wwill then not recieve the ollder datta unless the history is saved on a per account basis and only written to/marked after a secussfull upload
#ToDo: reporting services to tract history asnd trends, will require that the reports table be copyes to an archive table with a date stamp
#ToDo: add coloured output for errors to match the colour codes of the report
#ToDo: add function to skip account if segment is skip this will allow uploads but no downloads
#ToDo: add to report the number of records in the table after the import to make visible an issue if powerhsell downloads al of the records but downs not import them all
#ToDo: May need a Try/Catch for the download file loop as it is posible that errors are hapening here in responces, need to have it retry a few times befor reporting an error
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: 
#ToDo: